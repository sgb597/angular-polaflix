import { Component, OnInit } from '@angular/core';
import { SeriesService } from './series.service';

@Component({
  selector: 'app-agregar-serie',
  templateUrl: './agregar-serie.component.html',
  styleUrls: ['./agregar-serie.component.css']
})
export class AgregarSerieComponent implements OnInit {
  
  public series: {name: string,  description: string,  director: string, actors: string[]}[] = [];

  constructor(private seriesService: SeriesService) { }

  ngOnInit(): void {
    this.series = this.seriesService.getSeries();
  }

}
