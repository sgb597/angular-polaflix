import { Injectable } from '@angular/core';
import { Serie } from '../shared/serie.model';

@Injectable({
  providedIn: 'root'
})
export class SeriesService {

  private actores: string[] = [
    'Alejandro Moreno',
    'Carlos Perdomo',
    'Javier Dreikorn',
    'Charlie La Leyenda Rivera'
  ];

  private series: Serie[] = [
    new Serie('Serie1', 'Una serie de prueba!', 'Carlos Villanueva', this.actores),
    new Serie('Serie2', 'Una serie de prueba!', 'Guillermo DelToro', this.actores),
    new Serie('Serie3', 'Una serie de prueba!', 'Quentin Tarantino', this.actores),
    new Serie('Serie4', 'Una serie de prueba?', 'Martin Scorcese', this.actores)
  ];

  getSeries() {
    return this.series.slice();
  }

  constructor() { }
}
