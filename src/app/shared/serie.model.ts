export class Serie {
    constructor(public name: string, public description: string, public director: string, public actors: string[]) {}
}