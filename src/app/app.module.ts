import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { AgregarSerieComponent } from './agregar-serie/agregar-serie.component';
import { VerCargosComponent } from './ver-cargos/ver-cargos.component';
import { UsersComponent } from './users/users.component';
import { PlaylistComponent } from './home/playlist/playlist.component';
import { SeriesService } from './agregar-serie/series.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    AgregarSerieComponent,
    VerCargosComponent,
    UsersComponent,
    PlaylistComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [SeriesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
