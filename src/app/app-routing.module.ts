import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AgregarSerieComponent } from './agregar-serie/agregar-serie.component';
import { VerCargosComponent } from './ver-cargos/ver-cargos.component';
import { UsersComponent } from './users/users.component';


const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'series', component: AgregarSerieComponent},
  { path: 'cargos', component: VerCargosComponent},
  { path: 'users', component: UsersComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
